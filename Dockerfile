FROM ubuntu:16.04

MAINTAINER Alexander Kusakin <alexander.a.kusakin@gmail.com>

ENV LANG       en_US.UTF-8
ENV LC_ALL	   "en_US.UTF-8"
ENV LANGUAGE   en_US:en

RUN apt-get update
RUN apt-get install --no-install-recommends -y software-properties-common locales
RUN locale-gen en_US.UTF-8
RUN add-apt-repository ppa:ondrej/php
RUN apt-get update
RUN apt-get install --no-install-recommends -y --allow-unauthenticated curl sudo php5.6 php5.6-curl \
      php5.6-fpm php5.6-gd php5.6-json php5.6-mbstring php5.6-mcrypt \
      php5.6-opcache php5.6-sqlite3 php5.6-zip php-memcached \
      php-redis \
      php5.6-xml \
      php5.6-mysql \
      php5.6-odbc \
      php5.6-pgsql \
      ssmtp mailutils wget
RUN sed -i "s/listen = \/run\/php\/php5\.6-fpm\.sock/listen=0.0.0.0:9000/g" /etc/php/5.6/fpm/pool.d/www.conf
RUN sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 10M/g" /etc/php/5.6/fpm/php.ini
RUN sed -i "s/memory_limit = 128M/memory_limit = 256M/g" /etc/php/5.6/fpm/php.ini
RUN sed -i "s/short_open_tag = Off/short_open_tag = On/g" /etc/php/5.6/fpm/php.ini
RUN sed -i "s/short_open_tag = Off/short_open_tag = On/g" /etc/php/5.6/cli/php.ini
RUN echo "opcache.memory_consumption=128m" >> /etc/php/5.6/fpm/conf.d/10-opcache.ini

RUN mkdir -p /run/php/
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/*

RUN ln -sf /dev/stdout /var/log/php5.6-fpm.log

WORKDIR /etc/php

RUN wget https://downloads.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz
RUN tar xvf ioncube_loaders_lin_x86-64.tar.gz
RUN echo "zend_extension = /etc/php/ioncube/ioncube_loader_lin_5.6.so" >> /etc/php/5.6/fpm/conf.d/01-ioncube.ini
RUN echo "zend_extension = /etc/php/ioncube/ioncube_loader_lin_5.6.so" >> /etc/php/5.6/cli/conf.d/01-ioncube.ini


EXPOSE 9000

CMD ["/usr/sbin/php-fpm5.6", "--nodaemonize", "--force-stderr"]
